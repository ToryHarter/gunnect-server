﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace ETG_Gunnect_Server
{
    class GunnectServer
    {
        public static int MaxPlayers { get; private set; }
        public static int Port { get; private set; }

        public static Dictionary<int, GunnectClient> Clients = new Dictionary<int, GunnectClient>();
        public static Dictionary<int, Dictionary<string, ModData>> ClientModData = new Dictionary<int, Dictionary<string, ModData>>();
        public delegate void PacketHandler(int fromClient, Packet packet);
        public static Dictionary<int, PacketHandler> PacketHandlers;

        internal static List<PlayerInfo> PlayerInfos { get => playerInfos; set => playerInfos = value; }
        private static List<PlayerInfo> playerInfos = new List<PlayerInfo>();

        private static TcpListener _tcpListener;

        public static void Start(int maxPlayers, int port)
        {
            MaxPlayers = maxPlayers;
            Port = port;

            Console.WriteLine("Starting Gunnect+ Server...");
            InitializeServerData();

            _tcpListener = new TcpListener(IPAddress.Any, Port);
            _tcpListener.Start();
            _tcpListener.BeginAcceptTcpClient(new AsyncCallback(TCPConnectCallback), null);

            Console.WriteLine($"Server started on {Port}.");
        }

        private static void TCPConnectCallback(IAsyncResult result)
        {
            TcpClient client = _tcpListener.EndAcceptTcpClient(result);
            _tcpListener.BeginAcceptTcpClient(new AsyncCallback(TCPConnectCallback), null);
            Console.WriteLine($"Incoming connection from {client.Client.RemoteEndPoint}...");

            for (int i = 1; i <= MaxPlayers; i++)
            {
                if (Clients[i].Tcp.Socket == null)
                {
                    Clients[i].Tcp.Connect(client);
                    return;
                }
            }

            Console.WriteLine($"{client.Client.RemoteEndPoint} failed to connect: Server full!");
        }

        private static void InitializeServerData()
        {
            for (int i = 1; i <= MaxPlayers; i++)
            {
                Clients.Add(i, new GunnectClient(i));
                ClientModData[i] = new Dictionary<string, ModData>();
            }

            PacketHandlers = new Dictionary<int, PacketHandler>()
            {
                { (int)ClientPackets.WelcomeReceived, GunnectServerHandle.WelcomeReceived },
                { (int)ClientPackets.DynamicClientMessage, GunnectServerHandle.DynamicClientMessage },
                { (int)ClientPackets.UpdateClientModDataMessage, GunnectServerHandle.UpdateClientModDataMessage },
            };
            Console.WriteLine("Initialized packets.");
        }
    }
}
