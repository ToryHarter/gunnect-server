﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETG_Gunnect_Server
{
    class GunnectServerHandle
    {
        public static void WelcomeReceived(int fromClient, Packet packet)
        {
            int clientIdCheck = packet.ReadInt();
            string username = packet.ReadString();
            string remoteEndPoint = GunnectServer.Clients[fromClient].Tcp.Socket.Client.RemoteEndPoint.ToString();

            Console.WriteLine($"{remoteEndPoint} connected successfully and is now player {fromClient} ({username}).");

            GunnectServer.PlayerInfos.Add(new PlayerInfo
            {
                Id = fromClient,
                Username = username,
                RemoteEndPoint = remoteEndPoint
            });

            ServerSend.UpdateClientModDataMessage("Gunnect+ Client");

            CheckIfWrongClientIdAssumed(fromClient, clientIdCheck);
        }

        public static void DynamicClientMessage(int fromClient, Packet packet)
        {
            int clientIdCheck = packet.ReadInt();
            SendType sendType = (SendType)packet.ReadInt();
            List<int> toClients = JsonConvert.DeserializeObject<List<int>>(packet.ReadString());
            string modId = packet.ReadString();
            string dataTypeName = packet.ReadString();
            string data = packet.ReadString();

            ServerSend.DynamicServerMessage(clientIdCheck, sendType, toClients, modId, dataTypeName, data);

            CheckIfWrongClientIdAssumed(fromClient, clientIdCheck);
        }

        public static void UpdateClientModDataMessage(int fromClient, Packet packet)
        {
            int clientIdCheck = packet.ReadInt();
            string modId = packet.ReadString();
            string serializedModData = packet.ReadString();

            var modData = JsonConvert.DeserializeObject<ModData>(serializedModData);
            GunnectServer.ClientModData[fromClient][modId] = modData;

            ServerSend.UpdateClientModDataMessage(modId);

            CheckIfWrongClientIdAssumed(fromClient, clientIdCheck);
        }

        public static string GetSerializedPlayerInfo()
        {
            return JsonConvert.SerializeObject(GunnectServer.PlayerInfos);
        }

        private static void CheckIfWrongClientIdAssumed(int fromClient, int clientIdCheck)
        {
            if (fromClient != clientIdCheck)
            {
                Console.WriteLine($"Player (ID: {fromClient}) has assumed the wrong client ID ({clientIdCheck})!");
            }
        }
    }
}
