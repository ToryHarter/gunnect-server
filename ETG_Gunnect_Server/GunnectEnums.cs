﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETG_Gunnect_Server
{
    public enum SendType
    {
        All = 0,
        AllExcludingSender = 1,
        FromList = 2
    }
}
