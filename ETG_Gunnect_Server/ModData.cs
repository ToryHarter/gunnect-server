﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETG_Gunnect_Server
{
    class ModData
    {
        public string SerializedData { get; set; }
        public List<string> UIEntries { get; set; }
    }
}
