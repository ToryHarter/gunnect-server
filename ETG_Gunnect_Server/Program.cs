﻿using System;
using System.Threading;

namespace ETG_Gunnect_Server
{
    // All this copied from: https://www.youtube.com/watch?v=uh8XaC0Y5MA
    class Program
    {
        public const int MAX_PLAYERS = 4;
        public const int PORT = 26951;
        public const int TICKS_PER_SECOND = 30;
        public const int MILLISECONDS_PER_TICK = 1000 / TICKS_PER_SECOND;

        private static bool _isRunning = false;

        static void Main(string[] args)
        {
            Console.Title = "Gunnect+ Server";
            _isRunning = true;

            Thread mainThread = new Thread(new ThreadStart(MainThread));
            mainThread.Start();

            GunnectServer.Start(MAX_PLAYERS, PORT);
        }

        private static void MainThread()
        {
            Console.WriteLine($"Main thread started. Running at {TICKS_PER_SECOND} ticks per second");
            DateTime nextLoop = DateTime.Now;

            while (_isRunning)
            {
                while (nextLoop < DateTime.Now)
                {
                    ThreadUpdate.Update();

                    nextLoop = nextLoop.AddMilliseconds(MILLISECONDS_PER_TICK);

                    if (nextLoop > DateTime.Now)
                    {
                        Thread.Sleep(nextLoop - DateTime.Now);
                    }
                }

            }
        }
    }
}
