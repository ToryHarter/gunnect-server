﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETG_Gunnect_Server
{
    class PlayerInfo
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string RemoteEndPoint { get; set; }
    }
}
