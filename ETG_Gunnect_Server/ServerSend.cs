﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETG_Gunnect_Server
{
    class ServerSend
    {
        private static void SendTCPData(int toClient, Packet packet)
        {
            packet.WriteLength();
            GunnectServer.Clients[toClient].Tcp.SendData(packet);
        }

        private static void SendTCPDataToAll(Packet packet)
        {
            packet.WriteLength();
            for (int i = 1; i <= GunnectServer.MaxPlayers; i++)
            {
                GunnectServer.Clients[i].Tcp.SendData(packet);
            }
        }

        private static void SendTCPDataToAll(int exceptClient, Packet packet)
        {
            packet.WriteLength();
            for (int i = 1; i <= GunnectServer.MaxPlayers; i++)
            {
                if (i != exceptClient)
                {
                    GunnectServer.Clients[i].Tcp.SendData(packet);
                }
            }
        }

        public static void Welcome(int toClient, string msg)
        {
            using (Packet packet = new Packet((int)ServerPackets.Welcome))
            {
                packet.Write(msg);
                packet.Write(toClient);

                SendTCPData(toClient, packet);
            }
        }

        public static void DynamicServerMessage(int fromClient, SendType sendType, List<int> toClients, string modId, string dataTypeName, string data)
        {
            using (Packet packet = new Packet((int)ServerPackets.DynamicServerMessage))
            {
                packet.Write(fromClient);
                packet.Write(modId);
                packet.Write(dataTypeName);
                packet.Write(data);

                var fromClientUsername = GunnectServer.PlayerInfos.FirstOrDefault(x => x.Id == fromClient).Username;
                switch (sendType)
                {
                    case SendType.All:
                        Console.WriteLine($"{fromClientUsername} sending message of type {dataTypeName} from source {modId} to all players");

                        SendTCPDataToAll(packet);
                        break;
                    case SendType.AllExcludingSender:
                        Console.WriteLine($"{fromClientUsername} sending message of type {dataTypeName} from source {modId} to all players excluding themselves");

                        SendTCPDataToAll(fromClient, packet);
                        break;
                    case SendType.FromList:
                        if (toClients != null)
                        {
                            foreach (var toClient in toClients)
                            {
                                var toClientUsername = GunnectServer.PlayerInfos.FirstOrDefault(x => x.Id == toClient).Username;
                                Console.WriteLine($"{fromClientUsername} sending message of type {dataTypeName} from source {modId} to {toClientUsername}");

                                SendTCPData(toClient, packet);
                            }
                        }
                        break;
                    default:
                        return;
                }
            }
        }

        public static void UpdateClientModDataMessage(string modId)
        {
            try
            {
                using (Packet packet = new Packet((int)ServerPackets.UpdateClientModDataMessage))
                {
                    packet.Write(modId);
                    packet.Write(JsonConvert.SerializeObject(GunnectServer.PlayerInfos));
                    packet.Write(JsonConvert.SerializeObject(GunnectServer.ClientModData));

                    var modOrigin = modId == "" ? "Gunnect+ Client" : modId;
                    Console.WriteLine($"Sending updated client data to all players (Origin: {modOrigin})");

                    SendTCPDataToAll(packet);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
